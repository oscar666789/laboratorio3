Feature: This website

Scenario: As a user, I can log into a secure area

Given I am on the "login" page, 
When I login with my "<username>" and "<password>"
Then I should see a message saying "<message>"

Examples:
    | username  | password              | message |
    | tomsmith  | SuperSecretPassword!  | You logged into a secure area! |
    | foobar    | barfoo                | Your username is invalid! |